import React from "react";

function Task(props){
    return(
        <div className = "task"style = {{backgroundColor: props.claimed ? "green" : "white"}}>
          
          <h3>{props.taskName}</h3>
          <button onClick ={() => props.claimTask(props.id)}>Claim</button>
          <button onClick ={() => props.deleteTask(props.id)}> Completed </button>
        </div>
        );
 
}   
export default Task;