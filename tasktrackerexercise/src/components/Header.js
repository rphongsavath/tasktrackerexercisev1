import React from "react"

function Header() {
    return(
        <div className = "title">
            <h1>Task Tracker</h1>
        </div>
    );
}
export default Header;