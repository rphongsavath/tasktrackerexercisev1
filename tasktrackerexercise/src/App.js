
import './App.css';
import {useState} from "react";
import Task from "./components/Task";
import Header from "./components/Header";



function App() {

  const initialTasks = [
    {id: "1", taskName:"Send Discord greeting message"},
    {id: "2", taskName:"Daily stand up"},
    {id: "3", taskName:"Make coffee"},
    {id: "4", taskName:"Pull from GitHub Desktop"},
    {id: "5", taskName:"Wipe down monitors"},
    {id: "6", taskName:"Watch YouTube Tutorial"},
    {id: "7", taskName:"Check Assignment Calendar"},
    {id: "8", taskName:"Code 10 minutes"},

  ];
  const [todoList, setTodoList] = useState(initialTasks);
  const [newTask, setNewTask] = useState("");
  const [claimedTasks, setClaimedTasks] = useState([]);


  const addTask = () =>{
    const task = {
      id: todoList.length === 0 ? 1 : todoList[todoList.length-1].id + 1,
      taskName: newTask,
      claimed: false,
    }
    const newTodoList = [...todoList, task]
    setTodoList(newTodoList);
  };

  const deleteTask = (id) => {
    setTodoList(todoList.filter ((task) => task.id !== id));
    const updatedClaimedTasks = claimedTasks.filter(task =>task.id!==id);
    setClaimedTasks(updatedClaimedTasks);

  };
 
  const claimTask = (id) => {
      //duplicate task to claimed List
      const item = todoList.find(task => task.id ===id);
      setClaimedTasks([...claimedTasks,item]);
      //delete task selected task from TodoList *not working atm
      const updatedTodoTask = todoList.filter(task =>task.id!==id);
      setTodoList(updatedTodoTask);

  };

  const handleChange = (event) =>{
    setNewTask(event.target.value);
  };

  return (
    <div classname="App">
      
       <div className = "addTask">
        <input onChange = {handleChange}/>
        <button onClick={addTask}>Add Task</button>
      </div> 
      
      {/*Unclaimed Tasks list*/}
      <div className = "unclaimedList">
        <h2>Avaiable Tasks</h2>
        { todoList.map((task) =>{
          return(
          <Task 
            id={task.id} 
            taskName={task.taskName} 
            claimed={task.claimed}
            deleteTask={deleteTask}
            claimTask={claimTask}
          />
          );
        })}
      </div>


      {/*Claimed Tasks list*/}
      <div className = "associateList">
        <h2>Associate Todo List</h2>
        { claimedTasks.map((task) =>{
        return(
         <Task 
           id={task.id} 
           taskName={task.taskName} 
           claimed={task.claimed}
           deleteTask={deleteTask}
           />
          );
        })}
      </div>    



    </div>
  );
}

export default App;
