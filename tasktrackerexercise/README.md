## Project Name: 
Task Tracker Exercise

## Project Decription: 
This application allows users to track a list of tasks. Users will be able to claim available tasks to thier personal todo list. Users will be able to mark tasks as Completed as well.

## Deployment
1. Copy the application from the GitLab repository
2. Navigate to the application folder
3. Right click in the folder and select "Git Bash Here"
4. Navigate up one folder
5. Type 'npm start' in the Git Bash terminal and application should start local port 3000 in a seperate browser window. If port is already in use, type 'npx kill-port 3000' in the Git Bash terminal. Rerun 'npm start'.
6. Open application folder in Visual Studio to review code. 

## Author: Richy Phongsavath






